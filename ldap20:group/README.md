# ldap fet per Marc Sánchez
# ASX-m06 Curs 2020 - 2021

ldap20:group

Servidor ldap on hem afegit grups a la base de dades, amb usuaris pertanyents a aquests grups. El usuaris ja no
s'identifiquen per cn, ho fan per uid.

Ordre per arrencar:
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d isx48090422/ldap20:group

# Cal fer el dnf install dels paquets openldap
