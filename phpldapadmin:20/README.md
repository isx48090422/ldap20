# ldap fet per Marc Sánchez
# ASX-m06 Curs 2020 - 2021

phpldapadmin:20

servidor phpldapadmin connecta a al servidor ldap anomenat *ldap.edt.org* per accedir a les bases de dades *dc=edt,dc=org* i *cn=config*. Aquesta imatge està basada en fedora:27 per evitar el canvi de sintaxis de PHP 7.4.

Ordre per arrencar:
docker run --rm  --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d isx48090422/phpldapadmin:20

# Cal fer el dnf -y install phpldapadmin php-xml httpd

